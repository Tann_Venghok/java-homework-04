public class NullValueException extends Exception{
    public NullValueException(){
        super("The Value is Null");
    }
}
