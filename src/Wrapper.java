
import java.util.Arrays;
import java.util.List;

public class Wrapper<E extends Object>{
    private static int initialSize = 10;
    private int index;
    private int currentSize;
    private Object[] myArray;

    public Wrapper(){
        this.myArray = new Object[initialSize];
    }

    public void add(E e){
        checkForException(e);
        myArray[index] = e;
        index++;
        currentSize = myArray.length;

        //if array is full, then increase the size
        if(currentSize == index){
            increaseArraySize(currentSize);
        }
    }

    private void checkForException(E e) {
        if(e == null){
            try {
                throw new NullValueException();
            } catch (NullValueException ex) {
                System.out.println(ex);
            }
        }

        for(int i = 0; i < index; i++){
            if(e.equals(myArray[i]))
                try {
                    throw new DulplicateElementException();
                } catch (DulplicateElementException ex) {
                    System.out.println(ex);
                }
        }
    }

    public Object get(int i){
        if(i >= index)
            throw new IndexOutOfBoundsException("Index is out of bound.");
        return myArray[i];
    }

    public void printAll(){
        for (int i = 0; i < index; i++) {
            System.out.println(myArray[i]);
        }
    }

    public int size(){
        return index;
    }

    //double the size of array
    private void increaseArraySize(int currentSize) {
        myArray = Arrays.copyOf(myArray, currentSize * 2);
    }


}
